const constants = {
    formSwaggerResources: [
        {
            name: 'node-express',
            location: '/api-docs',
            swaggerVersion: '2.0'
        }
    ],
    companyName: 'Ajira Tech',
    companyUrl: 'http://www.ajira.tech'
};

global.appConstants = constants;
