/**
 * @swagger
 * definitions:
 *  companyDetailsResponse:
 *      type: object
 *      required:
 *          - companyId
 *      properties:
 *          companyId:
 *              type: integer
 *              format: int64
 *              description: Company Id
 *          name:
 *              type: string
 *              description: Company Name
 *
 *  employeeDetailsResponse:
 *      type: object
 *      required:
 *          - employeeId
 *      properties:
 *          employeeId:
 *              type: integer
 *              format: int64
 *              description: Employee Id
 *          name:
 *              type: string
 *              description: Employee Name
 *
 */