/* eslint newline-per-chained-call: "off" */

const createError = require('http-errors');
let service = require('./../services/employee-service');

/**
 * @swagger
 * /api/employee/{employeeId}:
 *  get:
 *      tags:
 *        - Employee
 *      summary: Get employee details
 *      description: Returns employee details
 *      produces:
 *        - application/json
 *      parameters:
 *        - name: employeeId
 *          in: path
 *          description: The ID of the employee
 *          required: true
 *          type: integer
 *          format: int64
 *      responses:
 *        200:
 *          description: Employee details associated with the employeeId
 *          schema:
 *            $ref: '#/definitions/employeeDetailsResponse'
 *        400:
 *          description: Bad request
 *        404:
 *          description: "Employee details not found"
 *        500:
 *          description: Internal server error
 */

function getEmployee(request, response, next) {
    const employeeId = request.params.employeeId;
    console.log("controller", employeeId);
    return service.getEmployee(employeeId)
        .then((result) => {
            console.log("result", result);
            response.status(200).json(result);
        }).then(() => next()).catch((err => next(err)));
}

/**
 * @swagger
 * /api/createEmployee:
 *  post:
 *      tags:
 *        - Employee
 *      summary: Create employee details
 *      operationId: Create employee
 *      description: Create employee details
 *      produces:
 *        - application/json
 *      parameters:
 *       - name: employee
 *         in: body
 *         description: The employee JSON you want to post
 *         schema:
 *      required: true
 *      responses:
 *        200:
 *          description: Create new employee
 *          schema:
 *            $ref: '#/definitions/employeeDetailsResponse'
 *        400:
 *          description: Bad request
 *        500:
 *          description: Internal server error
 */


// function create(request, response, next) {
//     var name = request.body;
//     console.log(name);
//     return service.create(name)
//         .then((result) => {
//             response.status(200).json(result);
//         }).then(() => next()).catch((err => next(err)));
// }

function create(request, response, next) {
    const name = request.body.name;
    return service.checkEmployeeNameExists(name)
        .then((isExists) => {
            if (isExists) {
                return Promise.reject(createError(409, 'Employee already exists with this name'));
            }
            return Promise.resolve();
        }).then(() => {
            const name = request.body;
            return service.create(name)
                .then((result) => {
                    response.status(200).json(result);
                })
        }).then(() => next()).catch((err) => {
            response.status(409).json(err.message);
        });
}

/**
 * @swagger
 * /api/updateEmployee/{employeeId}:
 *  put:
 *      tags:
 *        - Employee
 *      summary: Update employee details
 *      description: Returns updated employee details
 *      produces:
 *        - application/json
 *      parameters:
 *        - name: employeeId
 *          in: path
 *          description: The ID of the employee
 *          required: true
 *          type: integer
 *          format: int64
 *        - name: employee
 *          in: body
 *          description: The employee JSON you want to update
 *          required: true
 *          type: string
 *          format: json
 *      responses:
 *        200:
 *          description: Update employee by id
 *          schema:
 *            $ref: '#/definitions/employeeDetailsResponse'
 *        400:
 *          description: Bad request
 *        404:
 *          description: "Employee details not found"
 *        500:
 *          description: Internal server error
 */

function update(request, response, next) {
    const employeeId = request.params.employeeId;
    var name = request.body;
    console.log("controller", employeeId);
    console.log(name);
    return service.update(employeeId, name)
        .then((result) => {
            console.log("result controller--->", result)
            response.status(200).json(result);
        }).then(() => next()).catch((err => next(err)));
}

/**
 * @swagger
 * /api/deleteEmployee/{employeeId}:
 *  delete:
 *      tags:
 *        - Employee
 *      summary: Delete employee details
 *      description: Delete employee details
 *      produces:
 *        - application/json
 *      parameters:
 *        - name: employeeId
 *          in: path
 *          description: The ID of the employee
 *          required: true
 *          type: integer
 *          format: int64
 *      responses:
 *        200:
 *          description: Delete employee by id
 *          schema:
 *            $ref: '#/definitions/employeeDetailsResponse'
 *        400:
 *          description: Bad request
 *        404:
 *          description: "Employee details not found"
 *        500:
 *          description: Internal server error
 */

function deleteEmployee(request, response, next) {
    const employeeId = request.params.employeeId;
    return service.deleteEmployee(employeeId)
        .then((result) => {
            response.status(200).json(result.toJSON());
        }).then(() => next()).catch((err => next(err)));
}

function getEmployees(request, response, next) {
    return service.getEmployees()
        .then((result) => {
            response.status(200).json(result);
        }).then(() => next()).catch((err => next(err)));
}





module.exports = {
    getEmployee,
    create,
    getEmployees,
    update,
    deleteEmployee
};
