/* eslint newline-per-chained-call: "off" */


const createError = require('http-errors');
let service = require('./../services/company-service');

/**
 * @swagger
 * /api/company/{companyId}:
 *  get:
 *      tags:
 *        - Company
 *      summary: Get company details
 *      description: Returns company details
 *      produces:
 *        - application/json
 *      parameters:
 *        - name: companyId
 *          in: path
 *          description: The ID of the company
 *          required: true
 *          type: integer
 *          format: int64
 *      responses:
 *        200:
 *          description: Company details associated with the companyId
 *          schema:
 *            $ref: '#/definitions/companyDetailsResponse'
 *        400:
 *          description: Bad request
 *        404:
 *          description: "Company details not found"
 *        500:
 *          description: Internal server error
 */

// function getCompany(request, response, next) {
//     var companyId = request.params.companyId;
//     return service.getCompany(companyId)
//         .then((result) => {
//             response.status(200).json(result);
//         }).then(() => next()).catch((err => next(err)));
// }

function getCompany(request, response, next) {
    const companyId = request.params.companyId;
    return service.checkCompanyIsExists(companyId)
        .then((isExists) => {
            if (isExists) {
                console.log("isExists", isExists);
                return Promise.reject(createError(409, `Company not exists with this id ${companyId}`));
            }
            return Promise.resolve();
        }).then(() => {
            console.log("id", companyId);
            return service.getCompany(companyId)
                .then((result) => {
                    console.log("result controller", result);
                    response.status(200).json(result);
                })
        }).then(() => next()).catch((err) => {
            response.status(409).json(err.message);
        });
}

/**
 * @swagger
 * /api/create:
 *   post:
 *     summary: Create a company
 *     operationId: create company
 *     tags:
 *       - Company
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: company
 *         in: body
 *         description: The company JSON you want to post
 *         schema:
 *     required: true
 *     responses:
 *       "201":
 *         description: The company has been created
 *       default:
 *         description: unexpected error
 *         schema:
 *           $ref: '#/definitions/Error'
 */

function create(request, response, next) {
    const name = request.body.name;
    console.log("name controller", name);
    return service.checkCompanyNameExists(name)
        .then((isExist) => {
            console.log("exists controller", isExist)
            if (isExist) {
                console.log("if", isExist)
                return Promise.reject(createError(409, 'Company already exists with this name'));
            }
            return Promise.resolve();
        }).then(() => {
            const name = request.body;
            return service.create(name)
                .then((result) => {
                    console.log("controller", result.toJSON());
                    response.status(200).json(result);
                })
        }).then(() => next()).catch((err) => {
            response.status(409).json(err.message);
        });
}


/**
 * @swagger
 * /api/update/{companyId}:
 *  put:
 *      tags:
 *        - Company
 *      summary: Update company details
 *      description: Returns updated company details
 *      produces:
 *        - application/json
 *      parameters:
 *        - name: companyId
 *          in: path
 *          description: The ID of the company
 *          required: true
 *          type: integer
 *          format: int64
 *        - name: company
 *          in: body
 *          description: The company JSON you want to update
 *          required: true
 *          type: string
 *          format: json
 *      responses:
 *        200:
 *          description: Update company by id
 *          schema:
 *            $ref: '#/definitions/companyDetailsResponse'
 *        400:
 *          description: Bad request
 *        404:
 *          description: "Company details not found"
 *        500:
 *          description: Internal server error
 */

function update(request, response, next) {
    var companyId = request.params.companyId;
    var name = request.body;
    return service.update(companyId, name)
        .then((result) => {
            response.status(200).json(result);
        }).then(() => next()).catch((err => next(err)));
}

/**
 * @swagger
 * /api/deleteCompany/{companyId}:
 *  delete:
 *      tags:
 *        - Company
 *      summary: Delete company details
 *      description: Delete company details
 *      produces:
 *        - application/json
 *      parameters:
 *        - name: companyId
 *          in: path
 *          description: The ID of the company
 *          required: true
 *          type: integer
 *          format: int64
 *      responses:
 *        200:
 *          description: Delete company by id
 *          schema:
 *            $ref: '#/definitions/companyDetailsResponse'
 *        400:
 *          description: Bad request
 *        404:
 *          description: "Company details not found"
 *        500:
 *          description: Internal server error
 */


// function deleteCompany(request, response, next) {
//     var companyId = request.params.companyId;
//     return service.deleteCompany(companyId)
//         .then((result) => {
//             response.status(200).json(result);
//         }).then(() => next()).catch((err => next(err)));
// }

function deleteCompany(request, response, next) {
    const companyId = request.params.companyId;
    return service.checkCompanyIsExists(companyId)
        .then((isExists) => {
            if (isExists) {
                return Promise.reject(createError(409, `Company not exists with this id ${companyId}`));
            }
            return Promise.resolve();
        }).then(() => {
            return service.deleteCompany(companyId)
                .then((result) => {
                    response.status(200).json(result);
                })
        }).then(() => next()).catch((err) => {
            response.status(409).json(err.message);
        });
}
// function createCompany(request, response, next) {
//     var name = request.body;
//     return service.createCompany(name)
//         .then((result) => {
//             console.log("controller", result.toJSON());
//             response.status(200).json(result);
//         }).then(() => next()).catch((err => next(err)));
// }


function getCompanies(request, response, next) {
    return service.getCompanies()
        .then((result) => {
            console.log("controller", result.toJSON());
            response.status(200).json(result);
        }).then(() => next()).catch((err => next(err)));
}




module.exports = {
    create,
    getCompanies,
    getCompany,
    update,
    deleteCompany
};
