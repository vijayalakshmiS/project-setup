/**
 * Route to expose health endpoints.
 */
const express = require('express');
const router = express.Router();

/**
 * Returns ok as health response
 */
router.get('/health', (req, res) => {
    res.send('Server Status : UP!...');
});

router.get('/', (req, res) => {
    res.send('Node + Express Framework');
});

module.exports = router;
