const Joi = require('joi');
const expressValidation = require('express-validation');

const idValidator = Joi.number().positive();
const nameValidator = Joi.string();

/**
 * This is a Joi validator object for thesaurus apis. Any Joi validation object related to thesaurus apis should be defined here.
 */
const employeeValidators = {
    getEmployeeDetailsValidator: {
        params: {
            employeeId: idValidator.required()
        }
    },

    createEmployeeDetailsValidator: {
        body: {
            name: nameValidator.required()
        }
    }
};

const companyValidators = {
    getCompanyDetailsValidator: {
        params: {
            companyId: idValidator.required()
        },

        createCompanyDetailsValidator: {
            body: {
                name: nameValidator.required()
            }
        }
    }
};

function getExpressValidator(validator) {
    validator.options = {
        // throw error at the first error.
        abortEarly: true
    };

    // Eg: expressValidation(anyJoiValidator) will be a callback that can be passed in thesaurus routes to validate the
    // incoming request object.
    return expressValidation(validator);
}

module.exports = {
    getEmployeeDetailsValidator: getExpressValidator(employeeValidators.getEmployeeDetailsValidator),
    createEmployeeDetailsValidator: getExpressValidator(employeeValidators.createEmployeeDetailsValidator),
    getCompanyDetailsValidator: getExpressValidator(companyValidators.getCompanyDetailsValidator)
    // createCompanyDetailsValidator: getExpressValidator(companyValidators.createCompanyDetailsValidator)
};
