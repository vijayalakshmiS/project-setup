let express = require('express');
let router = express.Router();

let employeeController = require('./../controllers/employee-controller');
let companyController = require('./../controllers/company-controller');

const requestValidator = require('./validation/request-validator');

router.use('/', ((req, res, next) => {
    const contentType = req.headers['content-type'];
    if ((req.method === 'POST' || req.method === 'PUT') && (!contentType || contentType.indexOf('application/json') !== 0)) {
        return res.status(415).json({ message: 'content-type should be application/json' });
    }
    next();
}));


router.get('/employee/:employeeId', requestValidator.getEmployeeDetailsValidator, employeeController.getEmployee);
router.post('/createEmployee', requestValidator.createEmployeeDetailsValidator, employeeController.create);
router.get('/getEmployees', employeeController.getEmployees);
router.put('/updateEmployee/:employeeId', employeeController.update);
router.delete('/deleteEmployee/:employeeId', requestValidator.getEmployeeDetailsValidator, employeeController.deleteEmployee);

router.get('/company/:companyId', companyController.getCompany);
router.post('/createCompany', companyController.create);
router.get('/getCompanies', companyController.getCompanies);
router.put('/updateCompany/:companyId', companyController.update);
router.delete('/deleteCompany/:companyId', companyController.deleteCompany);



module.exports = router;
