/* global log b:true */

const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const ev = require('express-validation');
const validateErrors = require('./api/validation/validate-error');
const parsetrace = require('parsetrace');

require('./constants');
require('./configs/logging-autoconfiguration');

const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

require('./routes').initRoutes(app);
require('./helpers/swagger-docs').initRoutes(app);

app.use(validationError);
app.use(httpError);
app.use(logErrors);
app.use(app.get('env') !== 'server' ? devErrorHandler : serverErrorHandler);


function validationError(err, req, res, next) {
    if (err instanceof ev.ValidationError) {
        log.debug(err);
        res.status(err.status).json({ message: validateErrors(err) });
    } else {
        next(err);
    }
}

function httpError(err, req, res, next) {
    if (err.statusCode) {
        const errorObj = JSON.parse(err.error);
        log.error(errorObj);
        res.status(err.statusCode);
        res.json(errorObj);
    } else {
        next(err);
    }
}

function logErrors(err, req, res, next) {
    log.error(err);
    next(err);
}

function devErrorHandler(err, req, res, next) {
    res.status(err.status || 500);
    res.json({
        errorMessage: err.message,
        stackTrace: parsetrace(err, { sources: true }).object()
    });
}

function serverErrorHandler(err, req, res, next) {
    res.status(err.status || 500);
    res.json({
        errorMessage: err.message
    });
}

process.on('unhandledRejection', (reason) => {
    log.error(reason);
});

module.exports = app;
