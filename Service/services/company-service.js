/* global config */
const _ = require('lodash');
const Company = require('./db-provider').Company;

function getCompanyDetails(companyId) {
    return 'Company service call - list companies';
}

function create(name) {
    return new Company(name)
        .save()
        .then((result) => {
            console.log("service", result.toJSON());
            return result;
        });
}

function getCompanies() {
    return new Company().fetchAll({ withRelated: 'employees' })
        .then((result) => {
            console.log("service", result.toJSON());
            return result;
        });
}

function getCompany(companyId) {
    return new Company({ 'id': companyId }).fetch({ withRelated: 'employees' })
        .then((result) => {
            return result;
        });
}

function update(companyId, name) {
    return new Company({ 'id': companyId }).save(name)
        .then((result) => {
            return result;
        });
}

function deleteCompany(companyId) {
    return new Company({ 'id': companyId }).destroy()
        .then((result) => {
            return 'Company deleted';
        });
}

function checkCompanyNameExists(name) {
    console.log("service name", name)
    return new Company({ 'name': name }).fetch()
        .then((result) => {
            console.log("result", result);
            if (result != null) {
                return true;
            } else {
                return false;
            }

        });
}

function checkCompanyIsExists(companyId) {
    console.log("service id", companyId);
    return new Company({ 'id' : companyId }).fetch()
    .then((result) => {
        console.log("result", result);
        if (result == null) {
            return true
        }
        else {
            return false;
        }
    });
}

module.exports = {
    getCompanyDetails,
    create,
    getCompanies,
    getCompany,
    update,
    deleteCompany,
    checkCompanyNameExists,
    checkCompanyIsExists
};
