/* global config */

const Employee = require('./db-provider').Employee;

function getEmployee(employeeId) {
    console.log("employeeId", employeeId);
    return new Employee({ 'id': employeeId }).fetch()
        .then((result) => {
            console.log(result);
            return result;
        });
}

function create(name) {
    console.log("name", name)
    return new Employee(name)
        .save()
        .then((result) => {
            console.log(result.toJSON());
            return result;
        });
}

function getEmployees() {
    return new Employee().fetchAll()
        .then((result) => {
            console.log(result.toJSON());
            return result;
        });
}

// function updateEmployee(employeeId, name) {
// 	return new Employee({ 'id' : employeeId }).fetch()
// 	.then((result) => {
// 		console.log("result service", result.toJSON());
// 		 return result.save(name).then((updated) => {
// 			console.log("updated", updated.toJSON());
// 			return updated;
// 		});
// 	});
// }

function update(employeeId, name) {
    return new Employee({ 'id': employeeId }).save(name)
        .then((result) => {
            console.log("result service", result.toJSON());
            return result;
        });
}

function deleteEmployee(employeeId) {
    return new Employee({ 'id': employeeId }).destroy()
        .then((result) => {
            console.log(result.toJSON());
            return result;
        });

}

function checkEmployeeNameExists(name) {
    return new Employee({ 'name': name }).fetch()
        .then((result) => {
            if (result != null) {
                return true;
            } else {
                return false;
            }
        });
}

module.exports = {
    getEmployee,
    create,
    getEmployees,
    update,
    deleteEmployee,
    checkEmployeeNameExists
};
