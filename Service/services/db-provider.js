/* global log b:true */

const config = require('../connections');
const bookshelf = require('bookshelf')(config);

let Company = bookshelf.Model.extend({
    tableName: 'company',
    employees: function() {
        return this.hasMany(Employee);
    }
});

let Employee = bookshelf.Model.extend({
    tableName: 'employee',
    company: function() {
        return this.belongsTo(Company);
    }
});



module.exports = { Employee, Company };
