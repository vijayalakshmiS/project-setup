/**
 * Main application routes
 */

function initRoutes(app) {
    app.use('/api/', require('./api/services'));
    app.use('/', require('./api/health'));
}

module.exports = {
    initRoutes
};
